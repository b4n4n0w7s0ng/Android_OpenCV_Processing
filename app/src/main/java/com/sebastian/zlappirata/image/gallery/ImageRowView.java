package com.sebastian.zlappirata.image.gallery;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.sebastian.zlappirata.R;

/**
 * TODO: document your custom view class.
 */
public class ImageRowView extends LinearLayout {

    public Boolean isExpanded = false;
    public final static long animationTime = 3000;

    public ImageRowView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ImageRowView(Context context) {
        super(context);
    }


}
