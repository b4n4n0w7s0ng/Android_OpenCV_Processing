package com.sebastian.zlappirata.image.processing;

import android.graphics.Bitmap;

import org.jetbrains.annotations.NotNull;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

/**
 * TODO: on-demand OpenCV features activation - without it, let app
 * work, as if there was no image detection
 */
public class EdgeDetector {
    public Bitmap detectEdges(@NotNull Bitmap bmp) {
        Mat matrix = new Mat(bmp.getHeight(), bmp.getWidth(), CvType.CV_8UC3);
        Utils.bitmapToMat(bmp, matrix);

        Mat grayScale = new Mat(matrix.size(), CvType.CV_8UC1);
        Imgproc.cvtColor(matrix, grayScale, Imgproc.COLOR_RGB2GRAY);

        Mat edges = new Mat();
        Mat destination = new Mat();
        Imgproc.Canny(grayScale, edges, 80, 90);
        Imgproc.cvtColor(edges, destination, Imgproc.COLOR_GRAY2RGBA, CvType.CV_8U);

        Bitmap result = Bitmap.createBitmap(destination.cols(), destination.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(destination, result);
        return result;
    }
}
