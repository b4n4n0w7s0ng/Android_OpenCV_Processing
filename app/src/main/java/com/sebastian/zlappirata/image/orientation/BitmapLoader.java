package com.sebastian.zlappirata.image.orientation;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;

import java.io.IOException;

public class BitmapLoader {

    public Bitmap getBitmap(String photoPath) {
        return BitmapFactory.decodeFile(photoPath);
    }

    /**
     * Weird fix. Using BMP Exif file seems not to work.
     * We rotate image either way.
     *
     * @param photoPath - path to image file
     * @return Bitmap
     */
    public Bitmap getNormalizedBitmap(String photoPath){
        Bitmap bitmap = getBitmap(photoPath);
        return rotateImage(bitmap, 90);
    }

    /**
     * Use simple image rotation.
     * @param image         - image to rotate
     * @param rotationAngle - angle to rotate bitmap by
     *
     * @return Bitmap
     */
    private Bitmap rotateImage(Bitmap image, float rotationAngle) {
        // set rotation matrix (Image Processing)
        Matrix matrix = new Matrix();
        matrix.postRotate(rotationAngle);

        // transform by Matrix
        return Bitmap.createBitmap(image, 0, 0, image.getWidth(), image.getHeight(),
                matrix, true);
    }

}
