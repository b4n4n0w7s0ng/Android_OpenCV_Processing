package com.sebastian.zlappirata.image.gallery;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.sebastian.zlappirata.R;

import java.util.ArrayList;

public class ImageList extends ArrayAdapter<ImageRow> {
    private Context context;
    private ArrayList<ImageRow> rows;
    public ImageList(@NonNull Context context, ArrayList<ImageRow> list) {
        super(context,  0, list);

        this.context = context;
        this.rows = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View row =  convertView;
        if (row == null) {
            row = LayoutInflater.from(context).inflate(R.layout.image_row_view, parent, false);
        }

        ImageRow currentRow = rows.get(position);


        return super.getView(position, convertView, parent);
    }
}
