package com.sebastian.zlappirata.image.gallery;

import java.util.Date;

public class ImageRow {

    private Date takenDate;
    private String origPath;
    private String processedPath;

    public ImageRow(String origPath, String processedPath) {
        this.takenDate = new Date();
        this.origPath = origPath;
        this.processedPath = processedPath;
    }

    public String getProcessedPath() {
        return processedPath;
    }

    public void setProcessedPath(String processedPath) {
        this.processedPath = processedPath;
    }

    public String getOrigPath() {
        return origPath;
    }

    public void setOrigPath(String origPath) {
        this.origPath = origPath;
    }

    public void setTakenDate(Date takenDate) {
        this.takenDate = takenDate;
    }

    public Date getTakenDate() {
        return takenDate;
    }
}
