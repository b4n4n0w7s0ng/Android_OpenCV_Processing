package com.sebastian.zlappirata.files;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.text.format.DateFormat;

import java.io.File;
import java.io.IOException;
import java.util.Date;

public class FileStorage {

    public final static String imageVariableName = "PHOTO_PATH";
    private String fileExtension;
    private final static String filePrefix = "ZlapPirata";


    private File storageDirectory;
    public String imagePath;
    private String appPackageName;

    public FileStorage(File storageDir, String packageName) {
        fileExtension = ".jpg";
        storageDirectory = storageDir;
        appPackageName = packageName;
    }

    /**
     * Create internal image object returning URI.
     *
     * @param context - application context
     * @return Uri to that file
     */
    public Uri createImageForUri(Context context) {
        File image;
        Uri photoUri = null;

        try {
//            System.out.println("Is storage emulated? "+FileStorage.isStorageEmulated());

            if (!FileStorage.isExternalStorageReadable()) {
                throw new Exception("External storage not readable");
            }
            if (!FileStorage.isExternalStorageWritable()) {
                throw new Exception("External storage not writeable");
            }

            image = this.createImageFile();

            imagePath = image.getAbsolutePath();
            photoUri = FileProvider.getUriForFile(context, appPackageName, image);
        } catch(Exception ex) {
            System.err.println("Error: could not save file: "+ex.getMessage());
        }

        return photoUri;
    }

    public void setStorageDirectory(File storageDirectory) {
        this.storageDirectory = storageDirectory;
    }

    public File[] listFilesInStorageDirectory() {
        return storageDirectory.listFiles();
    }

    @NonNull
    private File createImageFile() throws IOException{
        String suffixTimeStamp;
        suffixTimeStamp = DateFormat.format("yyyyMMdd_HHmmss",new Date()).toString();
        String imageName = filePrefix +"_"+suffixTimeStamp;

        return File.createTempFile(imageName, fileExtension, storageDirectory);
    }

    /* Checks if external storage is available for read and write */
    private static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    /* Checks if external storage is available to at least read */
    private static boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state);
    }
}
