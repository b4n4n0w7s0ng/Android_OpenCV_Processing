package com.sebastian.zlappirata.navigation;

import android.animation.ObjectAnimator;
import android.support.v7.app.ActionBar;
import android.view.View;

import com.sebastian.zlappirata.R;

public class AppShellNav {
    private boolean toggled = false;

    private int animationDurationMilliseconds = 1000;

    public AppShellNav() {
    }

    /**
     * Animate toggling drawer menu.
     *
     * @param view - view to animate
     */
    public void toggleDrawerView(View view) {
        if (!toggled) {
            showDrawer(view);
        } else {
            hideDrawer(view);
        }
    }

    private void showDrawer(View drawerView) {
        toggled = true;
        ObjectAnimator animator = ObjectAnimator.ofFloat(drawerView, "translationX", 1f*drawerView.getWidth());
        animator.setDuration(animationDurationMilliseconds);
        animator.start();
    }

    public void hideDrawer(View view) {
        toggled = false;
        ObjectAnimator animator = ObjectAnimator.ofFloat(view, "translationX", 0f);
        animator.setDuration(animationDurationMilliseconds);
        animator.start();
    }

    /**
     * Set up drawer menu.
     *
     * @param actionBar - action bar to set up Home button Upon
     */
    public void setupToolbar(ActionBar actionBar) {

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
        }
    }

    public void hideOnCollapsed(View topView, View drawerView) {
        topView.setOnClickListener(v-> {
            if (toggled) {
                hideDrawer(drawerView);
            }
        });
    }
}
