package com.sebastian.zlappirata;

import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.sebastian.zlappirata.files.FileStorage;
import com.sebastian.zlappirata.navigation.AppShellNav;
import com.sebastian.zlappirata.views.GalleryFragment;
import com.sebastian.zlappirata.navigation.Navigation;
import com.sebastian.zlappirata.views.HomeFragment;
import com.sebastian.zlappirata.views.TakePhotoFragment;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;

import java.io.File;

public class AppShellActivity
        extends AppCompatActivity
        implements Navigation.OnFragmentInteractionListener
{
    private String activityName = "AppShellActivity";
    private AppShellNav appShellNav;
    private FragmentManager fragmentManager;
    String photoPath = "";

    /**
     * To be called on OpenCV init.
     */
    LoaderCallbackInterface openCvLoader = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch(status) {
                case LoaderCallbackInterface.SUCCESS:
                    // on loaded ...
                    Log.d(getClass().getName(), "OpenCV Loaded");
                    break;
                default:
                    super.onManagerConnected(status);
            }

        }
    };

    @Override
    public void onResume() {
        // use OpenCV Manager from Play Store
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0,this, openCvLoader);
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                View drawer = findViewById(R.id.drawer);
                appShellNav.toggleDrawerView(drawer);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Enable specific behavior and layout of action bar
     * to support more device models.
     *
     * @return
     */
    private ActionBar setupActionBar () {
        Toolbar toolbar = findViewById(R.id.custom_toolbar);
        setSupportActionBar(toolbar);

        return getSupportActionBar();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_shell);

        ActionBar actionBar = setupActionBar();
        appShellNav = new AppShellNav();
        appShellNav.setupToolbar(actionBar);
        appShellNav.hideOnCollapsed(findViewById(R.id.top_layout), findViewById(R.id.drawer));

        fragmentManager = getSupportFragmentManager();
        onNavigationItemSelected(R.id.navigation_home);
        File storageDirectory = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (storageDirectory != null) {
            photoPath = storageDirectory.getAbsolutePath();
        }
    }


    @Override
    public void onNavigationItemSelected(int resourceId) {
        Fragment targetFragmentView;

        Bundle args = new Bundle();
        switch(resourceId) {
            default: // no break
            case R.id.navigation_home:
                targetFragmentView = new HomeFragment();
                break;
            case R.id.navigation_processing:
                targetFragmentView = new TakePhotoFragment();
                args.putString(FileStorage.imageVariableName, photoPath);
                targetFragmentView.setArguments(args);
                break;
            case R.id.navigation_gallery:
                targetFragmentView = new GalleryFragment();
                args.putString(FileStorage.imageVariableName, photoPath);
                targetFragmentView.setArguments(args);
                break;
        }

        fragmentManager.beginTransaction()
                .replace(R.id.content_fragment, targetFragmentView)
                .commit();

        appShellNav.hideDrawer(findViewById(R.id.drawer));
    }
}
