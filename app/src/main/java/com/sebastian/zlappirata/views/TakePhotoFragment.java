package com.sebastian.zlappirata.views;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.sebastian.zlappirata.R;
import com.sebastian.zlappirata.files.FileStorage;
import com.sebastian.zlappirata.image.orientation.BitmapLoader;
import com.sebastian.zlappirata.image.processing.EdgeDetector;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Objects;

import static android.app.Activity.RESULT_OK;


/**
 *
 */
public class TakePhotoFragment extends Fragment {

    private final static int REQUIRE_CAPTURE_RESULT = 1;

    private ImageView iView;
    private Button processButton;
    private FileStorage storage;
    private String photoPath;



    private OnFragmentInteractionListener mListener;

    public TakePhotoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param photoPath
     * @return A new instance of fragment TakePhotoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TakePhotoFragment newInstance(String photoPath) {
        TakePhotoFragment fragment = new TakePhotoFragment();
        Bundle args = new Bundle();
        args.putString("PHOTO_PATH",photoPath);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle arguments = getArguments();
        if (arguments != null) {
            photoPath = arguments.getString(FileStorage.imageVariableName);
            if (photoPath != null) {
                File directory = new File(photoPath);
                storage.setStorageDirectory(directory);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_take_photo, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    /**
     * Process photo you just made using OpenCV package.
     */
    private void processImage() {
        BitmapLoader loader = new BitmapLoader();
        Bitmap imgBmp = loader.getBitmap(storage.imagePath);

        EdgeDetector detector = new EdgeDetector();
        Bitmap processed = detector.detectEdges(imgBmp);
        iView.setImageBitmap(processed);

        String targetImagePath = storage.imagePath.replaceAll("\\.jpg$", "_processed.jpg");
        try {
            FileOutputStream out = new FileOutputStream(targetImagePath);
            processed.compress(Bitmap.CompressFormat.JPEG, 100, out);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create new Intent for camera. Take photo.
     */
    private void takePhoto() {
        Uri photoUri = storage.createImageForUri(getContext());
        Intent getPictureIntent;

        if (photoUri != null) {
//            System.out.println(photoUri.toString());

            getPictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            getPictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);

            if (getPictureIntent.resolveActivity(Objects.requireNonNull(getContext()).getPackageManager()) != null) {
                startActivityForResult(getPictureIntent, REQUIRE_CAPTURE_RESULT);
            }
        }
    }

    /**
     * When camera activity is done for and we have the result.
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUIRE_CAPTURE_RESULT && resultCode == RESULT_OK) {
            BitmapLoader loader = new BitmapLoader();
            Bitmap bmp = loader.getBitmap(storage.imagePath);
            iView.setImageBitmap(bmp);
            processButton.setVisibility(View.VISIBLE);
        } // else, go for help

        Log.d(getClass().getName(),"PHOTO CAPTURED, process bitmap: https://stackoverflow.com/questions/20327213/getting-path-of-captured-image-in-android-using-camera-intent");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Button takePictureBtn = view.findViewById(R.id.takePictureButton);
        takePictureBtn.setOnClickListener(v -> takePhoto());

        iView = view.findViewById(R.id.pictureImage);
        processButton = view.findViewById(R.id.processPictureButton);
        processButton.setOnClickListener(v -> processImage());

        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        storage = new FileStorage(null, context.getPackageName());
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
