package com.sebastian.zlappirata.views.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;

import com.sebastian.zlappirata.R;

import com.sebastian.zlappirata.image.gallery.ImageRowView;

public class ImageViewHolder extends RecyclerView.ViewHolder {
    public ImageRowView imageRowView;

    public ImageViewHolder(View itemView) {
        super(itemView);
        imageRowView = itemView.findViewById(R.id.image_row);
    }




}