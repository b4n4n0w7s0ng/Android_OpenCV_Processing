package com.sebastian.zlappirata.views.holder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;
import android.widget.ImageView;

import com.sebastian.zlappirata.R;
import com.sebastian.zlappirata.image.gallery.ImageRow;
import com.sebastian.zlappirata.image.gallery.ImageRowView;
import com.sebastian.zlappirata.image.orientation.BitmapLoader;

import java.util.List;

public class GalleryItemAdapter extends RecyclerView.Adapter {
    private List<ImageRow> data;

    public GalleryItemAdapter(List<ImageRow> items) {
        data = items;
        // detect scroll and load next rows
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View imageRow = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.image_row_view, parent, false);

        return new ImageViewHolder(imageRow);
    }



    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ImageViewHolder itemHolder = (ImageViewHolder) holder;
        ImageRowView listElementView = itemHolder.imageRowView;

        ImageView firstImage  = listElementView.findViewById(R.id.origImage);
        ImageView secondImage = listElementView.findViewById(R.id.procImage);

        BitmapLoader loader = new BitmapLoader();

        firstImage.setImageBitmap(loader.getBitmap(data.get(position).getOrigPath()));
        secondImage.setImageBitmap(loader.getBitmap(data.get(position).getProcessedPath()));

        int height = holder.itemView.getHeight();
        holder.itemView.setOnClickListener((view) -> {
            ImageRowView v = view.findViewById(R.id.image_row);
            System.out.println("Has focus: " + v.isExpanded + ", view is " + view.getClass().getName());
            if (!v.isExpanded) {
                expand(v);
                v.isExpanded = true;
            } else {
                collapse(v);
                v.isExpanded = false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public void expand(View v) {
        animateExpandCollapse(v.getHeight(), 1.f,v);
    }

    public void collapse(View v) {
        animateExpandCollapse(v.getHeight(), -.5f,v);
    }

    private static void animateExpandCollapse(long targetHeight, float proportion, View v) {
        Animation animation = new Animation() {
            @Override
            public boolean willChangeBounds() {
                return true;
            }

            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                changeHeight((int)(targetHeight + targetHeight*(proportion * interpolatedTime)), v);
            }
        };
        animation.setDuration(ImageRowView.animationTime);
        v.startAnimation(animation);
    }

    private static void changeHeight(long targetHeight, View v) {
        v.getLayoutParams().height = (int) targetHeight;
        v.requestLayout();
    }
}
