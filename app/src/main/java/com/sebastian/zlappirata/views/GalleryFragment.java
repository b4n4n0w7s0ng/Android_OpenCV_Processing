package com.sebastian.zlappirata.views;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sebastian.zlappirata.R;
import com.sebastian.zlappirata.files.FileStorage;
import com.sebastian.zlappirata.views.holder.GalleryItemAdapter;
import com.sebastian.zlappirata.image.gallery.ImageRow;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link GalleryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GalleryFragment extends Fragment {

    private FileStorage storage;
    private LinearLayoutManager layoutManager;
    private RecyclerView.Adapter galleryAdapter;

    public GalleryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment GalleryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GalleryFragment newInstance() {
        GalleryFragment fragment = new GalleryFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null) {
            String photoPath = arguments.getString(FileStorage.imageVariableName);
            if (photoPath != null) {
                File directory = new File(photoPath);
                storage.setStorageDirectory(directory);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View inflatedContainer = inflater.inflate(R.layout.fragment_gallery, container, false);
        RecyclerView recyclerView = inflatedContainer.findViewById(R.id.galleryRecyclerView);

        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        List<ImageRow> rowsData = loadFilesIntoImageRowsList(storage);
        galleryAdapter = new GalleryItemAdapter(rowsData);

        recyclerView.setAdapter(galleryAdapter);

        return inflatedContainer;
    }

    private ArrayList<ImageRow> loadFilesIntoImageRowsList(FileStorage storage) {
        ArrayList<ImageRow> rowsData = new ArrayList<>();
        File[] files = storage.listFilesInStorageDirectory();

        for (File file : files) {
            // create pairs of matching original file and processed copy
            if (!file.getPath().endsWith("_processed.jpg")) {
                File processedFile = new File(file.getAbsolutePath().replace(".jpg", "_processed.jpg"));
                String processedPath = "";
                if (processedFile.exists()) {
                    processedPath = processedFile.getAbsolutePath();
                }

                ImageRow item = new ImageRow(file.getAbsolutePath(), processedPath);
                rowsData.add(item);
            }
        }
        return rowsData;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        storage = new FileStorage(null, context.getPackageName());
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
