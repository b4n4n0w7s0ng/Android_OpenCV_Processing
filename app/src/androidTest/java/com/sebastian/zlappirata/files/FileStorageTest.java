package com.sebastian.zlappirata.files;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.support.test.InstrumentationRegistry;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FileStorageTest {
    public FileStorageTest() {
//        super();
    }

    @Test
    public void getImageFileUri() {
        // Context of the app under test.
        Context context = InstrumentationRegistry.getTargetContext();
        assertEquals(getClass().getPackage().getName(), "com.sebastian.zlappirata.files");
        FileStorage fs = new FileStorage(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), getClass().getPackage().getName());
        Uri uri = fs.createImageForUri(context);
        assertEquals(uri.toString(), context.getPackageName());
    }
}
